/** @format */

import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/app';
import { YellowBox, I18nManager } from 'react-native';
import {name as appName} from './app.json';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
I18nManager.allowRTL(false);

const Application = () => {
    return ( <App/>
    );
};
AppRegistry.registerComponent(appName, () => App);







