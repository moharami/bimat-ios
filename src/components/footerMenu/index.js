import React from 'react';
import {Text, View, TouchableOpacity, AsyncStorage, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Axios from 'axios'
import AlertModal from './alertModal';
export const url = 'https://bimat.ir/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

export default class FooterMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            loading: false,
            modalVisible: false
        };
    }
    test(item) {
        console.log('herre is footer menu')
        this.setState({modalVisible: true});
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);
                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                console.log('expiresTime', expiresTime);
                console.log('currentTime', currentTime);
                if(expiresTime> currentTime ){
                    // Actions.home({openDrawer: this.props.openDrawer, loged: true})
                    Actions.profile({openDrawer: this.props.openDrawer})
                    this.setState({modalVisible: false});
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, profile: true})
                    this.setState({modalVisible: false});
                    // this.setState({info: true});
                }
            }
            else{
                Actions.login({openDrawer: this.props.openDrawer, profile: true});
                this.setState({modalVisible: false});
            }
        });
    }
    render(){
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.test('profile')}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'profile' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'profile' ? 2: 0}]}>
                        <FIcon name="user" size={30} color={this.props.active === 'profile' ? 'rgb(0, 121, 255)': 'gray'} />
                        <Text style={{color: this.props.active === 'profile' ? 'rgb(0, 121, 255)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >پروفایل</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.insuranceBuy()}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'bime' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'bime' ? 2: 0}]}>
                        <FIcon name="shield" size={40} color={this.props.active === 'bime' ? 'rgb(0, 121, 255)': 'gray'} />
                        <Text style={{color:this.props.active === 'bime' ? 'rgb(0, 121, 255)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >خرید بیمه</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>  Actions.home({openDrawer: this.props.openDrawer})}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'blog' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'blog' ? 2: 0}]}>
                        <Icon name="leaf" size={30} color={this.props.active === 'blog' ? 'rgb(0, 121, 255)': 'gray'} />
                        <Text style={{color: this.props.active === 'blog' ? 'rgb(0, 121, 255)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>بلاگ</Text>
                    </View>
                </TouchableOpacity>
                <AlertModal
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    modelCats={this.state.modelCats}
                />
            </View>
        );
    }
}