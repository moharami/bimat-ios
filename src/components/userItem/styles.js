

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        height: 47,
        backgroundColor: 'white',
        paddingRight: 10,
        paddingLeft: 10,
        paddingBottom: 1

    },
    left: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    label: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    allTime: {
        width: '80%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center'
    },
    timeContainer: {
        // width: '70%',
        width: '50%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',

    }
});