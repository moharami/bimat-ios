import React from 'react';
import {Text, View, InteractionManager, ScrollView, Image} from 'react-native';

import styles from './styles'
import safe from '../../assets/safe.png'
import wallet from '../../assets/wallet.png'
import send from '../../assets/send.png'
import support from '../../assets/support.png'
import time from '../../assets/time.png'

export default class BuyHeader extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    render(){
        return (
            <ScrollView style={{ transform: [
                { scaleX: -1}

            ],}}
                        horizontal={true} showsHorizontalScrollIndicator={false}
                // onContentSizeChange={() => this._onContentSizeChange()}
            >
                <View style={styles.container}>
                    <View style={styles.navContainer}>
                        <Text style={styles.label}>صرفه جویی در وقت</Text>
                        <Image source={time} style={styles.image} />
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.label}>ارسال رایگان</Text>
                        <Image source={support} style={[styles.image, {width: 38}]} />
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.label}>حاملیت در همه مراحل</Text>
                        <Image source={send} style={[styles.image, {width: 42}]} />
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.label}>خرید آسان</Text>
                        <Image source={safe} style={[styles.image, {width: 58}]} />
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.label}>پرداخت امن</Text>
                        <Image source={wallet} style={styles.image} />
                    </View>

                </View>
            </ScrollView>
        );
    }
}