import React, {Component} from 'react';
import { View, TouchableOpacity, Text, KeyboardAvoidingView,ImageBackground, Dimensions, BackHandler, Alert, ScrollView, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios'
;
export const url = 'https://bimat.ir/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader';
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import FooterMenu from "../../components/footerMenu/index";
import UserItem from "../../components/userItem";
import MIcon from "react-native-vector-icons/dist/MaterialCommunityIcons"
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import moment_jalaali from 'moment-jalaali'
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'
import {store} from '../../config/store';
import moment from 'moment'

class UserInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false,
            imgSrc: null,
            logout: false,
            fname: '',
            lname: '',
            father_name: '',
            national_id: '',
            birthday: '',
            phone: '',
            email: '',
            post_code: '',
            province: '',
            showPicker: false,
            selectedStartDate: null,
            sex: '',
            bDate: '',
            changed: false,
            modalVisible: false,
            national: false,
            inputDetect: false,
            postalMassage: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false, selectedtSartDate: date})}, 200)
        // this.setState({ selectedtSartDate: date });
        console.log('dddlldlld',moment_jalaali(date).format('jYYYY/jM/jD'))
    }
    // componentWillUnmount() {
    //     BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    // }
    onBackPress(){
        Actions.pop();
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    updateInfo() {
        const input = this.state.national_id === '' ? this.props.user.national_id : this.state.national_id;
        const input2 = this.state.post_code === '' ? this.props.user.post_code : this.state.post_code;
        console.log('input2', input2)
        console.log('this.props.user.national_id', this.props.user.national_id)
        if (!/^\d{10}$/.test(input))
            this.setState({national: true, modalVisible: true, loading: false});
        let check = parseInt(input[9],10);
        let sum = [0, 1, 2, 3, 4, 5, 6, 7, 8]
                .map(function (x) {
                    return parseInt(input[x],10) * (10 - x);
                })
                .reduce(function (x, y) {
                    return x + y;
                }) % 11;

        // if ((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11)){
        //     this.setState({nationalIdValidation: true}, () => {console.log('nationalIdValidation', this.state.nationalIdValidation)});
        // }
        if (!((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11))){
            this.setState({national: true, modalVisible: true, loading: false});
        }
        else if(this.state.post_code !== '' && input2.length !== 10 ||  this.state.post_code !== '' &&this.state.post_code.length !== 10) {
            this.setState({modalVisible: true, postalMassage: true, nationalIdMassage: false, national: false });
        }
        else {
            if (((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11))){
                this.setState({national: false, modalVisible: false, loading: false});
            }
            this.setState({loading: true});
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                // Axios.defaults.headers.common['Content-Type'] = 'multipart/form-data' ;
                let formdata = new FormData();
                formdata.append("fname", this.state.fname === '' ? this.props.user.fname : this.state.fname)
                formdata.append("lname", this.state.lname === '' ? this.props.user.lname : this.state.lname)
                formdata.append("father_name", this.state.father_name === '' ? this.props.user.father_name : this.state.father_name)
                formdata.append("national_id", this.state.national_id === '' ? this.props.user.national_id : this.state.national_id)
                formdata.append("province", this.state.province === '' ? this.props.user.province : this.state.province)
                formdata.append("phone", this.state.phone === '' ? this.props.user.phone : this.state.phone)
                formdata.append("mobile", newInfo.mobile)
                formdata.append("email", this.state.email === '' ? this.props.user.email : this.state.email)
                formdata.append("sex", this.state.sex === '' ? this.props.user.sex : this.state.sex)
                formdata.append("post_code", this.state.post_code === '' ? this.props.user.post_code : this.state.post_code)
                formdata.append("birthday", this.state.birthday === '' ? this.props.user.birthday : moment(this.state.selectedStartDate).format('Y-M-D'))
                if(this.state.imgSrc !== null) {
                    formdata.append("type", "image");
                    formdata.append("file", {
                        uri: this.state.imgSrc,
                        type: 'image/jpeg',
                        name: 'file'
                    });
                }
                Axios.post('/set_profile_final', formdata
                ).then(response=> {
                    if(response.data.msg === 'ProfileSuccess'){
                        console.log('edited item', response.data);
                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        this.setState({changed: true, modalVisible: true, loading: false});
                    }
                })
                .catch((response) => {
                    if(response.response.data.msg === 'ErorrInput'){
                        this.setState({
                            loading: false
                        }, () => {
                            this.setState({inputDetect: true, modalVisible: true});
                        })
                    }
                    else {
                        this.setState({modalVisible: true, loading: false});
                    }
                });

            });
        }
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    imgSrc: response.uri
                })
            }
        });
    }
    logoutMethod() {
        AsyncStorage.removeItem('token');
        AsyncStorage.removeItem('loged');

        Alert.alert(
            'خروج از حساب کاربری',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {Actions.insuranceBuy();})},
            ]
        )
    }
    openPicker() {
        this.setState({ showPicker: true });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    checkTextInput(fill, num){
        switch (num) {
            case 1:
                this.setState({fname: fill});
                break;
            case 2:
                this.setState({lname: fill});
                break;
            case 3:
                this.setState({father_name: fill});
                break;
            case 4:
                this.setState({national_id: fill});
                break;
            case 5:
                this.setState({birthday: fill});
                break;
            case 6:
                this.setState({phone: fill});
                break;
            case 7:
                this.setState({email: fill});
                break;
            case 8:
                this.setState({province: fill});
                break;
            case 9:
                this.setState({post_code: fill});
                break;
            case 10:
                this.setState({sex: fill});

                break;
            default:
                break;
        }
    }
    render() {
        // console.log('uuuuuser', this.props.user)
        const {user} = this.props;
        console.log('userrrrrrrr,', this.props.user)
        // console.log('this.props.user.birthday === null',this.props.user.birthday === "null")
        // this.state.selectedStartDate!== null && console.log('moment_jalaali(this.state.selectedStartDate).format()',moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD'))
        if(this.state.loading){
            return (<Loader />)
        }
        else
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <LinearGradient
                    start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>
                    <View style={styles.image} source={background}>
                        <View style={styles.imageRow}>
                            <TouchableOpacity onPress={() => this.logoutMethod()}>
                                <MIcon name="logout" size={26} color="white" />
                            </TouchableOpacity>
                            <View style={styles.profile}>
                                <Text style={styles.name}>{this.props.user.fname} {this.props.user.lname}</Text>
                                <Text style={styles.tel}>{this.props.user.mobile}</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.updateInfo()}>
                                <Icon name="check" size={22} color="white" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
                {/*<LinearGradient*/}
                    {/*start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>*/}
                    <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
                {/*</LinearGradient>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <Text style={styles.label}>اطلاعات فردی(الزامی)</Text>
                        <View style={{backgroundColor: 'white', borderRadius: 10}}>
                            <UserItem label="نام" border check={(fill) => this.checkTextInput(fill, 1)} value={this.props.user.fname} />
                            <UserItem label="نام خانوادگی" border check={(fill) => this.checkTextInput(fill, 2)} value={this.props.user.lname} />
                            <UserItem label="کد ملی" border check={(fill) => this.checkTextInput(fill, 4)} value={this.props.user.national_id !== "null" ? this.props.user.national_id : 'کد ملی'} />
                            <UserItem label="جنسیت" sex border check={(fill) => this.checkTextInput(fill, 10)} status={this.props.user.sex === 'female'? 'female':(this.props.user.sex === 'male'? 'male' : null) }  />
                            {/*<UserItem label="تاریخ تولد" check={(fill) => this.checkTextInput(fill, 5)} value={this.state.selectedStartDate !== null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : (this.props.user.birthday !== "null" ? moment_jalaali(this.props.user.birthday).format('jYYYY/jM/jD') :null)} birthday openPicker={() => this.openPicker()} date={this.state.selectedStartDate}/>*/}
                            {/*<UserItem label="تاریخ تولد" check={(fill) => this.checkTextInput(fill, 5)} value={this.state.selectedStartDate !== null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : null} birthday openPicker={() => this.openPicker()} date={this.state.selectedStartDate}/>*/}
                        </View>
                        <Text style={[styles.label, {paddingTop: 20}]}>اطلاعات تماس(اختیاری)</Text>
                        <View style={{backgroundColor: 'white', borderRadius: 10}}>
                            <UserItem label="شماره موبایل" border check={(fill) => this.checkTextInput(fill, 6)} value={this.props.user.mobile !== "null" ? this.props.user.mobile : 'شماره موبایل'} />
                            <UserItem label="ایمیل" border check={(fill) => this.checkTextInput(fill, 7)}  value={this.props.user.email !== "null" ? this.props.user.email : 'ایمیل'} />
                            <UserItem label="استان" province border check={(fill) => this.checkTextInput(fill, 8)} value={this.props.user.province !== "null" ? this.props.user.province : 'استان'} />
                            <UserItem label="کد پستی" border check={(fill) => this.checkTextInput(fill, 9)} value={this.props.user.post_code!== "null" ? this.props.user.post_code : 'کد پستی'} />
                            <UserItem label="نام پدر" border check={(fill) => this.checkTextInput(fill, 3)} value={this.props.user.father_name !== "null" ? this.props.user.father_name : 'نام پدر'} />
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    width: '100%',
                                    position: 'absolute',
                                    bottom: '30%',
                                    right: '5%',
                                    left: '5%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PersianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}

                            title={this.state.changed ? 'پروفایل با موفقیت تغییر یافت' :(this.state.national ? 'لطفا کد ملی را صحیح وارد کنید':(this.state.inputDetect ? 'لطفا موارد الزامی را پر نمایید': (this.state.postalMassage ? 'کد پستی باید 10 رقمی باشد': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' )))}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>

            </KeyboardAvoidingView>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(UserInfo);