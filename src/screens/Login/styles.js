import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'space-between',
        backgroundColor:'rgb(244, 244, 244)',
        paddingTop: 50

    },
    imageContainer: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        top: 60,
        right: 0,
        left: 0
    },
    image: {
        // width: '100%',
        // position: 'absolute',
        // // top: 60,
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // top: 80,
        // zIndex: 9999,

    },
    row: {
        width: '90%',
        flexDirection:'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        // padding: 8
        paddingLeft: 8

    },
    timerContainer: {
        width: 80,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        paddingRight: 15,
        paddingLeft: 15,
        borderRadius: 5
    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 100
    },
    header:{
        paddingBottom: 10,
        color: '#333640',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 13,
        alignSelf:'flex-end',
        paddingLeft:10,
        paddingTop: '23%'
    },
    label: {
        paddingBottom: 10,
        color: '#7A8299',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 13,
        alignSelf:'flex-end',
        paddingLeft:10,
    },
    advertise: {
        borderRadius:6,
        width: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'white',
        padding: 8,
        marginTop: 10

    },
    buttonTitle: {
        fontSize: 13,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    send: {
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    footerContainer: {
        width: '100%',
        marginBottom: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 20
    },
    footer: {
        flexDirection: 'row'
    },
    footerText: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    signup: {
        fontSize: 12,
        color: 'rgba(17, 103, 253, 1)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingTop: 20
    }

});
