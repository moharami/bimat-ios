
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    bodyContainer: {
        paddingBottom: 180,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 30
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 30,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(255, 193, 39, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        paddingRight: 10,
        paddingLeft: 10,
        elevation: 4,
        width: 50,
        height: 50,
    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,
    },
    topLabel: {
        paddingLeft: 15,
        paddingRight: 10,
        // color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    titleTop: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    body: {
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 5,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    left: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    innerLeft: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    right: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    innerRight: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    bodyLeft: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        paddingBottom: 7,
        color: 'rgba(122, 130, 153, 1)',
        textAlign: 'right'

    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'rgba(51, 54, 64, 1)',
        paddingBottom: 7,
        textAlign: 'right'

    },
    address: {
        width: '30%',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 8,
    }

});

