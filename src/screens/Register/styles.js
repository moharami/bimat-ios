import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor:'white',
        paddingTop: 160,
        position: 'relative',
        zIndex: 1
    },
    imageContainer: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        top: 60,
        right: 0,
        left: 0
    },
    image: {
        // width: '100%',
        // position: 'absolute',
        // // top: 60,
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // top: 80,
        // zIndex: 9999,

    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 100,
    },
    header:{
        paddingBottom: 10,
        color: '#333640',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 13,
        alignSelf:'flex-end',
        paddingLeft:10,
        paddingTop:'12%'
    },
    title:{
        // paddingBottom: 10,
        color: '#333640',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        paddingRight: 13,
        alignSelf:'flex-end',
        paddingLeft:10,
        paddingTop:10
    },
    label: {
        paddingBottom: 10,
        color: '#7A8299',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 13,
        alignSelf:'flex-end',
        paddingLeft:10,
    },
    advertise: {
        borderRadius:6,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 12,
        marginTop: 2

    },
    buttonTitle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    send: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    telpicker: {
        height: 40,
        backgroundColor: 'rgb(237, 237, 237)',
        width: '25%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10
    },
    telContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingLeft: '5%',
        marginTop: 20
    },

});
